function createNewUser() {

  let newUser = {
    firstName: prompt("What is your name?"),
    lastName: prompt("What is your surname?"),
    getLogin: function () {
      return ((this.firstName.charAt(0) + this.lastName).toLowerCase());
    },
    birthday: prompt("Please enter your date of birth", "dd.mm.yyyy"),

    getAge: function () {
      let now = new Date();
      let birthDate = new Date(Number(this.birthday.slice(6)), Number(this.birthday.slice(3, 5) - 1), Number(this.birthday.slice(0, 2)))
      return Math.floor((now - birthDate) / 1000 / 31536000);
    },

    getPassword: function () {
      return (this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6));
    }
  }

  Object.defineProperties(newUser, {
    firstName: { writable: false },
    lastName: { writable: false },
  })

  return newUser;
}

const user = createNewUser();

console.log("Object:", user);
console.log("Age:", user.getAge());
console.log("Password:", user.getPassword());
